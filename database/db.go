package database

import (
	"gitlab.com/Madaspe/opp-back/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var dsn = "host=db-postgresql-nyc3-35678-do-user-8810577-0.b.db.ondigitalocean.com user=doadmin password=gd3vuic7zglwqr2e dbname=defaultdb port=25060 sslmode=require"
var Db, _ = gorm.Open(postgres.Open(dsn), &gorm.Config{})

func CreateTables() {
	//Db.Migrator().DropTable(&models.Book{}, &models.User{}, &models.Message{}, &models.Card{})
	Db.AutoMigrate(&models.Book{}, &models.User{}, &models.Message{}, &models.Card{})
}
