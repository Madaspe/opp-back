module gitlab.com/Madaspe/opp-back

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-noodle/adapt v0.0.0-20171224161848-57eb9beabc2f
	github.com/go-noodle/middleware v0.0.0-20171224161924-90e544ca5b3d
	github.com/go-noodle/noodle v0.0.0-20180314081423-c90fdeb956a9 // indirect
	github.com/go-noodle/store v0.0.0-20171224161946-46cde2e548fd // indirect
	github.com/go-pg/pg/v10 v10.10.3
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	gopkg.in/tylerb/is.v1 v1.1.2 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12

)
