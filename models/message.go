package models

import "gorm.io/gorm"

type Message struct {
	gorm.Model
	Id        int64  `gorm:"PrimaryKey"`
	Text      string `json:"text"`
	Timestamp int64  `json:"timestamp"`
}
