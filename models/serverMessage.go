package models

type ServerMessage struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
}
