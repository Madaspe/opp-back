package models

import "gorm.io/gorm"

type Card struct { // Пример, нужно дополнить
	gorm.Model
	ID          int        `json:"id" gorm:"PrimaryKey"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	DeadLine    int64      `json:"dead_line"` // Дедлайн это просто timestamp, то есть количество секунд, которые прошли с 1940 года, поэтому int64
	Members     []*User    `json:"members" gorm:"many2many:cards_members"`
	Messages    []*Message `json:"messages" gorm:"many2many:cards_messages"`
}
