package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	ID         uint64     `gorm:"PrimaryKey"`
	FirstName  string     `json:"first_name"`
	SecondName string     `json:"second_name"`
	Username   string     `json:"username"`
	Password   string     `json:"password"`
	Messages   []*Message `json:"messages" gorm:"many2many:user_messages"`
}
