package models

import (
	"fmt"
	"gorm.io/gorm"
)

type Book struct {
	gorm.Model
	Id    int64  `json:"id" gorm:"PrimaryKey"`
	Title string `json:"title"`
}

func (u Book) String() string {
	return fmt.Sprintf("Book<%d %s>", u.Id, u.Title)
}
