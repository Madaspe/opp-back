package models

type NewMessage struct {
	Message  *Message `json:"message"`
	FromUser *User    `json:"from_user"`
	FromCard *Card    `json:"from_card"`
}
