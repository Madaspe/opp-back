package main

import (
	"github.com/go-noodle/adapt/gorilla"
	mw "github.com/go-noodle/middleware"
	"github.com/gorilla/mux"
	"gitlab.com/Madaspe/opp-back/database"
	"gitlab.com/Madaspe/opp-back/routers"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()

	database.CreateTables()
	n := mw.Default(gorilla.Vars)

	router.Handle("/", n.Then(routers.Index)).Methods("GET")
	// Book example
	router.Handle("/book/{id}", n.Then(routers.GetBook)).Methods("GET")
	router.Handle("/book", n.Then(routers.PostBook)).Methods("POST")
	router.Handle("/book/{id}", n.Then(routers.DeleteBook)).Methods("DELETE")
	router.Handle("/book", n.Then(routers.PutBook)).Methods("PUT")

	// User
	router.Handle("/user", n.Then(routers.GetUser)).Methods("GET")       // Авторизация
	router.Handle("/user", n.Then(routers.PostUser)).Methods("POST")     // Регистрация
	router.Handle("/user", n.Then(routers.DeleteUser)).Methods("DELETE") // Удаление (Только сам пользователь может удалить свой акк)
	router.Handle("/user", n.Then(routers.PutUser)).Methods("PUT")       // Смена пароль/логина (Тоже только сам пользователь)

	// Card
	router.Handle("/card/{id}", n.Then(routers.GetCard)).Methods("GET") // Получение карт
	router.Handle("/card", n.Then(routers.PostCard)).Methods("POST")    // Обновление

	router.Handle("/new_message", n.Then(routers.PostNewMessage)).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}
