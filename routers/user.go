package routers

import (
	"encoding/json"
	"gitlab.com/Madaspe/opp-back/database"
	"gitlab.com/Madaspe/opp-back/models"
	"gitlab.com/Madaspe/opp-back/utils"
	"golang.org/x/crypto/bcrypt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func GetUser(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	body, _ := ioutil.ReadAll(request.Body)
	user := new(models.User)

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(request.Body)

	err := json.Unmarshal(body, &user)

	if err != nil {
		log.Fatal(err)
	}

	var userExist models.User
	database.Db.Where(&models.User{Username: user.Username}).FirstOrInit(&userExist)

	err = bcrypt.CompareHashAndPassword([]byte(userExist.Password), []byte(user.Password))

	if err != nil {
		err := json.NewEncoder(responseWriter).Encode(models.ServerMessage{Message: "wrong password"})
		if err != nil {
			log.Fatal(err)
		}
		return
	}

	err = json.NewEncoder(responseWriter).Encode(userExist)
	if err != nil {
		return
	}

}

func PostUser(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	body, _ := ioutil.ReadAll(request.Body)
	user := new(models.User)

	defer func(Body io.ReadCloser) { // Body нужно закрыть. Defer выполняет функцию после return
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(request.Body)

	err := json.Unmarshal(body, &user) // Парсинг структуры из json`а из body

	if err != nil {
		log.Fatal(err)
	}

	user.Password, _ = utils.HashPassword(user.Password)

	var userExist models.User

	database.Db.Where(&models.User{Username: user.Username}).First(&userExist)

	if userExist.ID == 0 {
		database.Db.Create(&user)
	} else {
		err = json.NewEncoder(responseWriter).Encode(models.ServerMessage{
			Message:    "username exist",
			StatusCode: 200,
		})
		return
	}

	err = json.NewEncoder(responseWriter).Encode(models.ServerMessage{
		Message:    "created",
		StatusCode: 200,
	})
}

func DeleteUser(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

}

func PutUser(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

}
