package routers

import (
	"encoding/json"
	"gitlab.com/Madaspe/opp-back/database"
	"gitlab.com/Madaspe/opp-back/models"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func PostNewMessage(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	body, _ := ioutil.ReadAll(request.Body)
	newMessage := new(models.NewMessage)

	defer func(Body io.ReadCloser) { // Body нужно закрыть. Defer выполняет функцию после return
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(request.Body)

	err := json.Unmarshal(body, &newMessage)

	if err != nil {
		log.Fatal(err)
	}

	var user *models.User
	var card *models.Card

	database.Db.Where(&newMessage.FromCard).First(&card)
	database.Db.Where(&newMessage.FromUser).First(&user)

	user.Messages = append(user.Messages, newMessage.Message)

	card.Messages = append(card.Messages, newMessage.Message)
	card.Members = append(card.Members, user)

	go database.Db.Save(&user)
	go database.Db.Save(&card)

	err = json.NewEncoder(responseWriter).Encode(models.ServerMessage{Message: "ok"})
}
