package routers

import (
	"encoding/json"
	"github.com/go-noodle/adapt/gorilla"
	"gitlab.com/Madaspe/opp-back/database"
	"gitlab.com/Madaspe/opp-back/models"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func GetCard(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	var vars = gorilla.GetVars(request)
	id, _ := strconv.Atoi(vars["id"])

	var cardExist models.Card
	database.Db.Where(&models.Card{ID: id}).FirstOrInit(&cardExist)

	err := json.NewEncoder(responseWriter).Encode(cardExist)
	if err != nil {
		log.Fatal(err)
	}
	return
}

func PostCard(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

	body, _ := ioutil.ReadAll(request.Body)
	card := new(models.Card)

	defer func(Body io.ReadCloser) { // Body нужно закрыть. Defer выполняет функцию после return
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(request.Body)

	err := json.Unmarshal(body, &card) // Парсинг структуры из json`а из body

	if err != nil {
		log.Fatal(err)
	}
	database.Db.Create(&card)

	database.Db.First(&card)

	err = json.NewEncoder(responseWriter).Encode(card)
}

func DeleteCard(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

}

func PutCard(responseWriter http.ResponseWriter, request *http.Request) {
	responseWriter.Header().Set("Content-Type", "application/json")

}
